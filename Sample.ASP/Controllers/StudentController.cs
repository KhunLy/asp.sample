﻿using Sample.ASP.Models;
using Sample.DAL.Entities;
using Sample.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sample.ASP.Controllers
{

    // Les "controllers" sont les classes centrales du pattern MVC
    // C'est le point d'entrée des requêtes http
    // Leurs buts est de traiter les données qui proviennent des requêtes
    // Pour finalement générer une réponse (une vue la plupart du temps) 
    // qui sera renvoyée à l'auteur de la requète
    // Pour générer un vue elle se sert d'un modèle
    // Le modèle contient toutes les données dynamiques que la vue va afficher (le fond)
    // La vue définit un "template" pour afficher un modele particulier (la forme)
    public class StudentController : Controller
    {
        // Par défaut, pour atteindre une méthode d'un controller
        // Nous devons fournir le nom du controller, la méthode 
        // et un paramètre(facultatif) "id" dans nos url 

        // Route: /Student/Index
        public ActionResult Index()
        {
            // Nous souhaitons afficher la liste de tous les étudiants depuis cette méthode
            // Nous récupérons le service de connection à la table Student
            StudentRepository repo = new StudentRepository();

            // Nous appelons la méthode GetAll pour récupérer la liste de tous les étudiants
            IEnumerable<Student> students = repo.GetAll();

            // Nous n'utiliseront pas nos entités "db" pour travailler avec nos vues
            // Nous préfererons travailler plutôt avec nos models applicatifs
            // Nous voulons donc transformer ("Mapper") les instances de "Student"
            // en instances de "StudentModel"
            // nb: Nous utilisons ici la librairie linq pour ce "Mapping"
            // Dans le cadre du cours nous avons utiliser un "Mapper" générique
            IEnumerable<StudentModel> model = students.Select(s => new StudentModel
            {
                Id = s.Id,
                LastName = s.LastName,
                FirstName = s.FirstName,
                BirthDate = s.BirthDate,
            });
            // Nous fournissons finalement le model à une vue
            // pour créer notre réponse
            return View(model);

            // Par défaut, la vue retournée par les méthodes se
            // trouve dans un dossier qui porte le même nom que le controller de
            // la méthode et s'appelle "[nomDeLaMethode].cshtml"
            // Nous pouvons aussi générer automatiquement la vue
            // click droit -> Ajouter une vue
        }

        // Route /Student/Details/42
        public ActionResult Details(int id)
        {
            // cette méthode va me permettre d'afficher l'étudiant
            // possédant l'id passé en paramètre
            // Comme pour la méthode précédente, on récupère le "repository"
            StudentRepository repo = new StudentRepository();
            // Et on appelle la méthode get(int id) pour récupérer un étudiant
            // avec un id particulier
            Student st = repo.Get(id);
            // Comme pour la méthode précédente nous allons "Mapper"
            // la classe Student en StudentModel
            StudentModel model = new StudentModel()
            {
                Id = st.Id,
                LastName = st.LastName,
                FirstName = st.FirstName,
                BirthDate = st.BirthDate
            };
            return View(model);
        }
    }
}