﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sample.ASP.Models
{
    public class StudentModel
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime BirthDate { get; set; }
    }
}