﻿using Sample.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DAL.Repositories
{
    // Les "Repositories" sont des classes dites "services" dont le rôle est
    // de proposer est fournir des methodes qui permettront au applications (ASP, WPF, ...)
    // d'intéragir avec la db.
    // Dans cette exemple nous allons faire abstraction de la db, d'ADO 
    // et nous servir d'une liste statique
    public class StudentRepository : BaseRepository<Student>
    {
        // Cette liste privée ne nous sert qu'à stocker temporairement les données que nous allons enregistrer 
        private static List<Student> _FakeDB = new List<Student>()
        {
            new Student { Id = 1, LastName = "Ly", FirstName = "Khun", BirthDate = new DateTime(1982,5,6) },
            new Student { Id = 2, LastName = "Ly", FirstName = "Piv", BirthDate = new DateTime(1981,4,30) },
            new Student { Id = 3, LastName = "Person", FirstName = "Mike", BirthDate = new DateTime(1982,3,14) }
        };
        // Ici la variable privée sert de compteur pour autoincrémenter les ids
        private static int _LastId = 3;

        
        // Ici les méthodes que nous implémentons ne se connectent pas à la db
        // mais simule le traitement dans la liste "_FakeDB"
        public override Student Get(int id)
        {
            return _FakeDB.FirstOrDefault(s => s.Id == id);
        }

        public override IEnumerable<Student> GetAll()
        {
            return _FakeDB;
        }

        public override int Insert(Student entity)
        {
            entity.Id = ++_LastId;
            _FakeDB.Add(entity);
            return _LastId;
        }

        public override bool Update(Student entity)
        {
            Student toUpdate = _FakeDB.FirstOrDefault(s => s.Id == entity.Id);
            if (toUpdate != null)
            {
                toUpdate.LastName = entity.LastName;
                toUpdate.FirstName = entity.FirstName;
                toUpdate.BirthDate = entity.BirthDate;
                return true;
            }
            return false;
        }

        public override bool Delete(int id)
        {
            Student toDelete = _FakeDB.FirstOrDefault(s => s.Id == id);
            if(toDelete != null)
            {
                _FakeDB.Remove(toDelete);
                return true;
            }
            return false;
        }
    }
}
