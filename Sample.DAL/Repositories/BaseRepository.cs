﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DAL.Repositories
{
    // cette classe abtraite est la classe parente de tous les "repositories"
    // elle va contenir les propriétés et les méthodes communes
    // à tous les "Repositories"
    public abstract class BaseRepository<T>
    {
        // La Classe BaseRepository décrit souvent les fonctionalités communes qui sont
        // généralement implémentées dans les "Repositories" enfants
        public abstract T Get(int id);
        public abstract IEnumerable<T> GetAll();
        public abstract int Insert(T entity);
        public abstract bool Update(T entity);
        public abstract bool Delete(int id);
    }
}
